﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Homework1.Models
{
    public class Donate
    {
        [Display (Name = "First name")]
        [Required (ErrorMessage = "Please provide a first name")]
        public string FirstName { get; set; }

        [Display (Name = "Last name")]
        [Required (ErrorMessage = "Last name required")]
        public string LastName { get; set; }

        [Required (ErrorMessage = "Please provide an amount")]
        [DataType (DataType.Currency)]
        public double Amount { get; set; }

        [Display (Name = "Credit Card")]
        [Required (ErrorMessage = "Credit card number required")]
        [DataType (DataType.CreditCard)]
        public string CreditCard { get; set; }
    }
}