﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Homework1.Models
{
    public class Contact
    {
        [Required (ErrorMessage = "Provide a name")]
        [Display (Name = "Name")]
        public string Name { get; set; }

        [EmailAddress (ErrorMessage = "Provide an email address")]
        public string Email { get; set; }

        [Required (ErrorMessage = "Type a few words")]
        public string Comment { get; set; }
    }
}