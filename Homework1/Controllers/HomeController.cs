﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using Homework1.Models;

namespace Homework1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Contact()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Contact(Contact contact)
        {
            if(ModelState.IsValid)
            {
                try
                {
                    return View();
                }
                catch (Exception)
                {
                    return View();
                }
            }
            return View();
        }

        [HttpGet]
        public ActionResult About()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Departments()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Donate()
        {
            return View();
        }

        [HttpGet]
        public ActionResult Volunteer()
        {
            return View();
        }
    }
}